"""
@file
@author  Roland Loewe <rolo@env.dtu.dk>
@version 1.3
@section LICENSE
Copyright (C) 2022 Roland Loewe
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

#######################################################################################################
#######################################################################################################
###################Compute flood damages based on Raster and Landuse Shapefile#########################
#######################################################################################################
#########Author: Roland Loewe##########################################################################
#########Date: Jan 2022################################################################################
#######################################################################################################
#######################################################################################################
#read data for all time steps from a dfs2 or raster file###############################################
#analyse flood damages by converting damages and land use to raster####################################
#######################################################################################################
import os, sys, time
os.environ['PROJ_LIB'] = os.path.abspath(r'c:\Users\rolo\Anaconda3\envs\12124\Library\share\proj')
os.environ['GDAL_DATA'] = os.path.abspath(r'c:\Users\rolo\Anaconda3\envs\12124\Library\share')

import Functions
image_output=True #should rasters used for damage computation be saved as images for checking purposes?
wd=os.path.dirname(os.path.realpath(__file__))
p_config=os.path.join(wd,'config','config_damage_dfs2.xml')
workpath,resfile,bpath,rpath,gpath,extpath,build_typefield=Functions.read_data_config(p_config)
msdkpath=Functions.read_software_paths(p_config)
dfuncbuild, dfuncroad, dfuncgreen=Functions.read_damage_functions(p_config)
outputpath=os.path.join(workpath,'resfile.txt')
################PARAMETERS##############################################################################################
#the extent used for the analysis is defined by the extent layer - this means, that the pixels of the flood raster used for computing flood damages are not necessarily the same as the one provided, but they can be moved a bit to the side.
#if this effect is important, one needs to tune the resolution used for analysis
resolution=1.6 #defines resolution of the rasters used for computation of flood damages in m - make sure this is a multiple of the original flood raster resolution
#######################################################################################################
#location of the export script
sys.path.append(wd)
sys.path.append(msdkpath)
#################################################################
import shutil, gc, time, random
import numpy
from scipy import ndimage
from osgeo import gdal,gdalconst,ogr,osr
import preprocessing as pr
from helper_damage import *
from compute_damages import *

logpath=os.path.join(workpath,'Log.txt')
def write_log(path,text,mode='a'):
    f=open(path,mode)
    f.write(time.strftime("%d.%m %H:%M",time.localtime()) + ': ' + text + '\n')
    f.close()

def check_and_create_field(driver,path,nlayer,fieldname):
    #check if a field exists in the shp and create it otherwise (it seems not possible to create fields on memory datasets)
    src = driver.Open(path, 0)
    layer=src.GetLayer(nlayer)
    fnames=[layer.GetLayerDefn().GetFieldDefn(i).GetName() for i in range(layer.GetLayerDefn().GetFieldCount())]
    src.Destroy()
    src=None
    if not fieldname in fnames:
        src = driver.Open(path, 1)
        new_field=ogr.FieldDefn(fieldname, ogr.OFTInteger)
        layer=src.GetLayer(nlayer)
        layer.CreateField(new_field)
        src.Destroy()
        src=None

def read_data_from_shp(memdb,pshp,outlyr):
    driver = ogr.GetDriverByName("ESRI Shapefile")
    #if id field used later in code does not exist yet, create it on the shapefile
    check_and_create_field(driver,pshp,os.path.splitext(os.path.basename(pshp))[0],'tmp_fid')
    #read shape
    src_shp = driver.Open(pshp, 0)
    memlyr=memdb.CopyLayer(src_shp.GetLayer(),outlyr,['OVERWRITE=YES'])
    src_shp.Destroy()
    return memlyr

def read_data_from_sqlite(memdb,psqlite,lyr):
    driver = ogr.GetDriverByName('SQLite')
    src_db = driver.Open(psqlite, 0)
    memlyr=memdb.CopyLayer(src_db.GetLayer(lyr),lyr,['OVERWRITE=YES'])
    src_db.Destroy()
    return memlyr

def write_layer_to_gpkg(memdb,inlyr,outfile):    
    outdriver=ogr.GetDriverByName('GPKG')
    src_out=outdriver.CreateDataSource(outfile)
    input_layer=memdb.GetLayerByName(inlyr)
    src_out.CopyLayer(memdb.GetLayerByName(inlyr),inlyr,['OVERWRITE=YES'])
    src_out.Destroy()
    return 0

def get_building_areas(layer):
    fid=[]; area=[]
    for feature in layer: 
        fid.append(feature.GetField("tmp_fid"))
        area.append(feature.GetGeometryRef().GetArea())
    layer.ResetReading()
    return(fid,area)

try:
    #create workspace
    wspace=os.path.abspath(os.path.join(workpath,str(int(time.time())+random.randrange(0,int(1e6)))))
    if os.path.exists(wspace): shutil.rmtree(wspace)
    os.makedirs(wspace)
    write_log(logpath,'start work')
    #get source data from DANCE DB and catchment hull shapefile
    outdriver=ogr.GetDriverByName('MEMORY')
    source=outdriver.CreateDataSource('DataMEM')
    tmp=outdriver.Open('DataMEM',1)
    build_mem_all=read_data_from_shp(source,bpath,'build_all')
    street_mem=read_data_from_shp(source,rpath,'streets')
    green_mem=read_data_from_shp(source,gpath,'green')
    catch_hull_mem=read_data_from_shp(source,extpath,'extent')
    #clip buildings - often the building layer is much larger than the extent of interest
    write_log(logpath,'clip buildings')
    status=pr.clip_buildings(source,'build_all','extent')
    build_mem=source.GetLayer('building')
    srs = build_mem_all.GetSpatialRef()
    #create new FID field on land-use layers - sometimes layers have FID's with gaps or repetitions
    status=update_id_field(build_mem,'tmp_fid')
    status=update_id_field(street_mem,'tmp_fid')
    status=update_id_field(green_mem,'tmp_fid')
    ################################################################
    #get list of building tmp_fid's and associated polygon areas
    bids,bareas=get_building_areas(build_mem)
    #save the building layer for debugging
    _=write_layer_to_gpkg(source,'building',os.path.join(workpath,'work_buildings.gpkg'))
    ################################################################
    #read dfs2
    write_log(logpath,'read flood map')
    if 'dfs2' in resfile:
        import mike_IO_dfs2 as io_dfs2
        [status,XCount,YCount,xysize,Dx,Dy,X0,Y0,Lat,Lon,Or,WKT]=io_dfs2.get_spatial_reference(resfile)
        [status,blX,blY,trX,trY]=io_dfs2.get_extent_GDAL(WKT,Lon,Lat,Or, Dx, Dy, XCount, YCount)
        [status, dfsdata] = io_dfs2.read_dfs2_timestep(resfile, 1, 0)
        [status, flood_array] = io_dfs2.dfsdata_to_numpy2D(dfsdata,XCount,YCount) #gives 2D grid with first dimension = y and second dimension =x, origin in the topleft
        #flood_array[554-150,0] corresponds to [0,150] in the dfs2
        del dfsdata
    else:
        ds=gdal.Open(resfile)
        gt=ds.GetGeoTransform() #(ulx,Dx,RotX,uly,RotY,Dy) - where ulx and uly are in the top left corner of the pixel
        Dx=abs(gt[1])
        Dy=abs(gt[5])
        #if the original data have small grid sizes, aggregate to analysis resolution using max filter (e.g. when data come from SCALGO)
        if Dx<resolution:
            dsrs=ds.GetSpatialRef()
            warp_options = gdal.WarpOptions(dstSRS=dsrs,
                        xRes = resolution,yRes = resolution,
                        format = 'MEM',
                        #outputBounds=[point_wgs_bl[1],point_wgs_bl[0],point_wgs_tr[1],point_wgs_tr[0]],
                        resampleAlg='max')
            ds = gdal.Warp('', resfile, options = warp_options)
        gt=ds.GetGeoTransform() #(ulx,Dx,RotX,uly,RotY,Dy) - where ulx and uly are in the top left corner of the pixel
        Dx=abs(gt[1])
        Dy=abs(gt[5])      
        XCount=ds.RasterXSize
        YCount=ds.RasterYSize                
        blX=gt[0]
        blY=gt[3]-YCount*Dy
        trX=gt[0]+XCount*Dx
        trY=gt[3]
        flood_array=ds.GetRasterBand(1).ReadAsArray(0, 0, XCount, YCount).astype(float)
        del ds
        #EXPERIMENTAL - do gridding only for dfs2
        # flood_raster=ds.GetRasterBand(1)
        # resolution=Dx/resfactor #raster resolution for damage analysis - (Dx/RESOLUTION) MUST BE OF INTEGER TYPE!!!
        # status, np_flood=clip_raster_to_array(ds,catch_hull_mem,srs,resolution,0)
        # print("CHECK THAT FLOODMAP FLIP IS NEEDED AND NOT JUST FLOODSTROEM SPECIFIC")
        # np_flood=numpy.flipud(np_flood)
    #the max filter will run with size (3*Dx/RESOLUTION)x(3*Dx/RESOLUTION)
    #now pad 0's on the outer edges of the flood grid and modify the extent accordingly
    flood_array=numpy.lib.pad(flood_array, 1, 'constant', constant_values=0)
    blX=blX-Dx;blY=blY-Dy;trX=trX+Dx;trY=trY+Dy;XCount=XCount+2;YCount=YCount+2;xysize=XCount*YCount
    #now reshape to 1D, starting from top left taking one row in x-direction, before going to the next row down in y-direction
    flood_array=numpy.reshape(flood_array,xysize,'C')
    ################Create several grids with same extent and same resolution for damage computation
    ###start with flood results - convert flood depths to raster using nearest neighbor approach, assuming that flood depths are sampled at irregular intervals (the tool was developed to also cope with MIKE21 FM results)
    #compute coordinates according to the 1D flood array
    # ElementX=numpy.concatenate([numpy.arange(blX+0.5*Dx,blX+(0.5+XCount)*Dx,Dx) for j in range(YCount)])
    # ElementY=numpy.repeat(numpy.arange(trY-0.5*Dy,trY-(0.5+YCount)*Dy,-Dy),XCount)
    ElementX=numpy.tile(numpy.arange(blX+0.5*Dx,blX+(XCount+0.5)*Dx,Dx)[0:XCount],YCount)
    ElementY=numpy.repeat(numpy.arange(trY-0.5*Dy,trY-(YCount+0.5)*Dy,-Dy)[0:YCount],XCount)
    points=(ElementX,ElementY)
    del ElementX,ElementY
    #create a grid with extent of catch_hull from the flood array and land use layers
    write_log(logpath,'rasterize layers')
    status, np_flood=gridding_numpy_array(points,flood_array,catch_hull_mem,resolution,-9999)
    del points, flood_array
    if image_output:
        pathout=os.path.join(workpath,'flood.tiff')
        array_to_raster(np_flood,pathout,catch_hull_mem,srs,resolution,-9999)

    #now start comparing building and flood arrays against each other - as in old postprocessor
    #run a 3x3 max filter over the flood results, get pixels in the flood grid where we have 0 and a building, fill only these with the results of the filter
    write_log(logpath,'fill in flood raster')
    #convert all buildings to raster for use in flood area processing
    status,np_build_all=ogrlayer_to_array(build_mem,catch_hull_mem,srs,resolution,"tmp_fid",0)
    np_build_all=numpy.int32(np_build_all)
    from math import floor
    #filtersize=3*int(Dx/resolution) #old, wrong size
    filtersize=(floor(float(Dx)/float(resolution)/2)+1)*2
    if filtersize<3: filtersize=3
    np_flood_max=ndimage.maximum_filter(np_flood, size=filtersize, footprint=None, output=None, mode='constant', cval=0.0, origin=0)
    select=numpy.logical_and(np_flood<=0,np_build_all>500000)
    np_build_all=None
    np_flood_filled = numpy.empty_like(np_flood)
    np_flood_filled[:] = np_flood[:]
    np_flood_filled[select]=np_flood_max[select]
    np_flood_max=None
    select=None

    #compute damage for different building types
    damage_build=0
    for typeval in dfuncbuild:
        status, tmpbuildings = separate_building_types2(source,build_typefield,typeval)
        if status==1: raise
        status,np_tmpbuild=ogrlayer_to_array(tmpbuildings,catch_hull_mem,srs,resolution,"tmp_fid",0)
        if status==1: raise
        np_tmpbuild=numpy.int32(np_tmpbuild)
        write_log(logpath,'get flooded buildings')
        #get id of flooded buildings and the maximum water level in their neighborhoods
        #!!!!!!!!!!!check threshold*100 in count_flooded_buildings!!!!
        status, flood_b_id, flood_b_max = count_flooded_buildings(np_flood_filled,np_tmpbuild,0.01)
        if dfuncbuild[typeval][0]=='unit':
            damage_b, damage_b_list = compute_b_unit_damage(flood_b_max,dfuncbuild[typeval][1:])
            damage_build=damage_build+damage_b
            if image_output:
                pathout=os.path.join(workpath,'build_'+typeval+'.tiff')
                array_to_raster(np_tmpbuild,pathout,catch_hull_mem,srs,resolution,-1.0e-30)
        elif dfuncbuild[typeval][0]=='sqm':
            flood_barea=[[yy for xx,yy in zip(bids,bareas) if xx==x][0] for x in flood_b_id]
            damage_b, damage_b_list = compute_b_sqm_damage(flood_b_max,flood_barea,dfuncbuild[typeval][1:])
            damage_build=damage_build+damage_b
        else:
            write_log(logpath,'building damage function type must be either "unit" or "sqm"')
            sys.exit(1)
        #write results for each building into file for debugging
        reslogpath=os.path.splitext(logpath)[0]+'_'+typeval+'.txt'
        write_log(reslogpath,'\n'.join([str(x)+';'+str(y)+';'+str(z) for x,y,z in zip(flood_b_id,flood_b_max,damage_b_list)]),'w')
    np_flood_filled=None
    #compute street damage
    status=update_id_field_constant(street_mem,'tmp_fid')
    if status==1: raise
    status,np_street=ogrlayer_to_array(street_mem,catch_hull_mem,srs,resolution,"tmp_fid",0)
    np_street=numpy.int32(np_street)
    status, flood_rd_area = compute_flooded_area(np_flood,np_street,dfuncroad['ROADS'][1:][0][0],resolution)
    damage_rd = compute_sqm_damage(flood_rd_area,dfuncroad['ROADS'][1:])
    if image_output:
        pathout=os.path.join(workpath,'street.tiff')
        array_to_raster(np_street,pathout,catch_hull_mem,srs,resolution,-1.0e-30)
    del np_street

    #compute green area damage
    status=update_id_field_constant(green_mem,'tmp_fid')
    if status==1: raise
    status,np_green=ogrlayer_to_array(green_mem,catch_hull_mem,srs,resolution,"tmp_fid",0)
    np_green=numpy.int32(np_green)
    status, flood_g_area = compute_flooded_area(np_flood,np_green,dfuncgreen['GREEN'][1:][0][0],resolution)
    damage_g = compute_sqm_damage(flood_g_area,dfuncgreen['GREEN'][1:])
    if image_output:
        pathout=os.path.join(workpath,'green.tiff')
        array_to_raster(np_green,pathout,catch_hull_mem,srs,resolution,-1.0e-30)
    del np_green

    damage_tot=damage_build+damage_rd+damage_g

    #for checking
    text='total;buildings;roads;green\n'
    text=text+str(damage_tot) + ';' + str(damage_build) + ';' + str(damage_rd) + ';' + str(damage_g)
    Functions.writefile(outputpath,text)

    shutil.rmtree(wspace)
    gc.collect()
except:
    write_log(logpath,'result read failed')
    sys.exit(1)
write_log(logpath,'result read finished')
sys.exit(0)
