"""
@file
@author  Roland Loewe <rolo@env.dtu.dk>
@version 1.0
@section LICENSE
Copyright (C) 2016 Roland Loewe
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

   
def compute_b_unit_damage(list_maxdepth,damagefunction):
    #compute damage for residential buildings
    damage=0
    dam_build=[0]*len(list_maxdepth)
    for i in range(len(damagefunction[0])):
        current=damagefunction[0][i]
        if i<len(damagefunction[0])-1:
            nextval=damagefunction[0][i+1]
        else:
            nextval=1e10
        select=[x>=current and x<nextval for x in list_maxdepth]
        bcount=sum(select) #number of flooded buildings in this interval
        dam_build=[damagefunction[1][i] if x==True else y for x,y in zip(select,dam_build)]
        damage=damage+bcount*damagefunction[1][i]
    return damage, dam_build
    
def compute_b_sqm_damage(list_maxdepth,list_barea,damagefunction):
    #compute damage per sqm for buildings
    damage=0
    dam_build=[0]*len(list_maxdepth)
    for i in range(len(damagefunction[0])):
        current=damagefunction[0][i]
        if i<len(damagefunction[0])-1:
            nextval=damagefunction[0][i+1]
        else:
            nextval=1e10
        select=[x>=current and x<nextval for x in list_maxdepth]
        barea=sum([y for x,y in zip(select,list_barea) if x==True]) #total area flooded in this depth class
        dam_build=[z*damagefunction[1][i] if x==True else y for x,y,z in zip(select,dam_build,list_barea)]
        damage=damage+barea*damagefunction[1][i]
    return damage, dam_build
    
def compute_sqm_damage(flood_area,damagefunction):
    #compute damage for roads
    damage=flood_area*damagefunction[1][0]
    return damage