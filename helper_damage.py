"""
@file
@author  Roland Loewe <rolo@env.dtu.dk>
@version 1.0
@section LICENSE
Copyright (C) 2016 Roland Loewe
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
import os, sys
import numpy
from scipy.interpolate import griddata
from math import ceil

from osgeo import gdal,gdalconst,ogr,osr
#import clr
#clr.AddReference("DHI.Generic.MikeZero.DFS")
#clr.AddReference("DHI.Generic.MikeZero.EUM")
#clr.AddReference("System")
#
#import System
#from System import Array
#from DHI.Generic.MikeZero import eumUnit, eumItem, eumQuantity
#from DHI.Generic.MikeZero.DFS import *
#from DHI.Generic.MikeZero.DFS.dfs123 import *
##from DHI.Generic.Cartography import *
#from DHI.Generic.MikeZero.DFS.dfsu import *


def update_id_field(layer,fieldname):
    #runs through all features in layer, writes unique ID into the field specified by fieldname
    #assuming, that this field was available in the original data
    status=1
    i=1
    fid=0
    try:
        fcount=layer.GetFeatureCount()
        while i <= fcount:
            feature=layer.GetFeature(fid)
            fid=fid+1
            if feature is None: continue
            feature.SetField(fieldname,i*1000+500000) #set an identifier that we can later use for the flood computation
            layer.SetFeature(feature)
            i=i+1
        layer.ResetReading()
    except:
        return status
    status=0
    return status

def update_id_field_constant(slayer,fieldname):
    #runs through all features in slayer, writes unique ID into the field specified by fieldname
    status=1
    i=1
    fid=0
    try:
        new_field = ogr.FieldDefn(fieldname, ogr.OFTInteger)
        slayer.CreateField(new_field)
        fcount=slayer.GetFeatureCount()
        while i <= fcount:
            feature=slayer.GetFeature(fid)
            fid=fid+1
            if feature is None: continue
            feature.SetField(fieldname,500000) #set an identifier that we can later use for the flood computation
            slayer.SetFeature(feature)
            i=i+1
        slayer.ResetReading()
    except:
        return status
    status=0
    return status
    

def separate_building_types(src,typefield):
    #create separate building layers for residential and commercial/industrial
    status=1
    build=src.GetLayer('building')
    srs = src.GetLayer('building').GetSpatialRef()
    residential = src.CreateLayer("residential", srs, geom_type=ogr.wkbPolygon)
    commercial = src.CreateLayer("commercial", srs, geom_type=ogr.wkbPolygon)
    public = src.CreateLayer("public", srs, geom_type=ogr.wkbPolygon)
    layerdef = build.GetLayerDefn()
    #for i in range(layerdef.GetFieldCount()): print layerdef.GetFieldDefn(i).GetName()
    for i in range(layerdef.GetFieldCount()): 
        residential.CreateField(layerdef.GetFieldDefn(i))
        commercial.CreateField(layerdef.GetFieldDefn(i))
        public.CreateField(layerdef.GetFieldDefn(i))
    try:
        i=1
        fid=0
        fcount=build.GetFeatureCount()
        while i <= fcount:
            feature=build.GetFeature(fid)
            fid=fid+1
            if feature is None: continue
            i=i+1
            type=feature.GetField(typefield)
            if type is None or "RES" in type:
                residential.CreateFeature(feature)
            elif "COM" in type:
                commercial.CreateFeature(feature)
            elif "PUB" in type:
                public.CreateFeature(feature)
        build.ResetReading()
        residential.ResetReading()
        commercial.ResetReading()
        public.ResetReading()
        print('Res;Com;Pub' + str(residential.GetFeatureCount()) + '; ' +  str(commercial.GetFeatureCount()) + '; ' +  str(public.GetFeatureCount()))
    except:
        return status, None, None, None
    status=0
    return status, residential, commercial, public

def separate_building_types2(src,typefield,typeval):
    #create separate building layers for residential and commercial/industrial
    status=1
    build=src.GetLayer('building')
    srs = src.GetLayer('building').GetSpatialRef()
    lyrnames=[src.GetLayer(i).GetName() for i in range(src.GetLayerCount())]
    if 'tmp_build' in lyrnames: src.DeleteLayer(lyrnames.index('tmp_build'))
    tmpbuild = src.CreateLayer("tmp_build", srs, geom_type=ogr.wkbPolygon)
    layerdef = build.GetLayerDefn()
    #for i in range(layerdef.GetFieldCount()): print layerdef.GetFieldDefn(i).GetName()
    for i in range(layerdef.GetFieldCount()): 
        tmpbuild.CreateField(layerdef.GetFieldDefn(i))
    try:
        i=1
        fid=0
        fcount=build.GetFeatureCount()
        while i <= fcount:
            feature=build.GetFeature(fid)
            fid=fid+1
            if feature is None: continue
            i=i+1
            type=feature.GetField(typefield)
            if type ==typeval:
                tmpbuild.CreateFeature(feature)
        build.ResetReading()
        tmpbuild.ResetReading()
        print (typeval + ': ' + str(tmpbuild.GetFeatureCount()))
    except:
        return status, None
    status=0
    return status, tmpbuild
    
def ogrlayer_to_array(layer,extentlayer,srs,resolution,field,nodataval):
    #convert ogr vectorlayer to raster using GDAL rasterize
    #extent of the raster is defined from extent of extentlayer, gridsize through parameter resolution
    status=1
    try:
        pixelWidth = pixelHeight = resolution # depending how fine you want your raster
        x_min, x_max, y_min, y_max = extentlayer.GetExtent()
        cols = int((x_max - x_min) / pixelHeight)
        rows = int((y_max - y_min) / pixelWidth)
        ##see http://gis.stackexchange.com/questions/83251/convert-polygons-in-shape-file-to-a-numpy-array
        #target_ds = gdal.GetDriverByName('GTiff').Create('temp.tif', cols, rows, 1, gdal.GDT_Byte) 
        src_drv = gdal.GetDriverByName('MEM')
        target_ds = src_drv.Create('', cols, rows, 1, gdal.GDT_Float32)
        target_ds.SetGeoTransform((x_min, pixelWidth, 0, y_min, 0, pixelHeight))
        target_ds.SetProjection(srs.ExportToWkt())
        band = target_ds.GetRasterBand(1)
        band.SetNoDataValue(nodataval)
        band.FlushCache()
        gdal.RasterizeLayer(target_ds, [1], layer, options = ["ATTRIBUTE="+field])
        data=target_ds.ReadAsArray()
        #src_drv.Destroy()
        src_drv=None
    except:
        return status, None
    status=0
    return status, data

def array_to_raster(array,pathout,extentlayer,srs,resolution,nodataval):
    #convert numpy array to ogr raster defined by pathout
    #we assume that an extentlayer is provided, which is used to provide information on raster origin
    #extentlayer is assumed to be the same as when reading the flood grid
    status=1
    try:
        pixelWidth = pixelHeight = resolution # depending how fine you want your raster
        x_min, x_max, y_min, y_max = extentlayer.GetExtent()
        cols = int((x_max - x_min) / pixelHeight)
        rows = int((y_max - y_min) / pixelWidth)
        ##see http://gis.stackexchange.com/questions/83251/convert-polygons-in-shape-file-to-a-numpy-array
        src_drv=gdal.GetDriverByName('GTiff')
        if os.path.exists(pathout): os.remove(pathout)
        target_ds = src_drv.Create(pathout, cols, rows, 1, gdal.GDT_Float32) 
        #src_drv = gdal.GetDriverByName('MEM')
        #target_ds = src_drv.Create('', cols, rows, 1, gdal.GDT_Float32)
        target_ds.SetGeoTransform((x_min, pixelWidth, 0, y_min, 0, pixelHeight))
        target_ds.SetProjection(srs.ExportToWkt())
        band = target_ds.GetRasterBand(1)
        band.SetNoDataValue(nodataval)
        band.WriteArray(array)
        band.FlushCache()
        target_ds=None
        band=None
        src_drv=None
    except:
        return status
    status=0
    return status

def clip_raster_to_array(sourceds,extentlayer,srs,resolution,nodataval):
    #clip/extend raster layer to the extent of extentlayer - used to ensure that different datasets align
    #extent of the raster is defined from extent of extentlayer, gridsize through parameter resolution
    status=1
    try:
        pixelWidth = pixelHeight = resolution # depending how fine you want your raster
        x_min, x_max, y_min, y_max = extentlayer.GetExtent()
        cols = int((x_max - x_min) / pixelHeight)
        rows = int((y_max - y_min) / pixelWidth)
        gdaloptions=gdal.TranslateOptions(xRes=resolution,yRes=resolution,resampleAlg=gdalconst.GRA_NearestNeighbour,outputType=gdal.GDT_Float32,format='MEM',projWin=[x_min,y_min+rows*resolution,x_min+cols*resolution,y_min])
        outds=gdal.Translate('tmpraster2', sourceds,options=gdaloptions)    
        data=outds.ReadAsArray()
    except:
        print('clip_raster_to_array failed')
        return status, Nones
    status=0
    return status, data

def array_to_raster_arcpy(array,pathout,extentlayer,srs,resolution,nodataval):
    #NOT TESTED
    #convert numpy array to raster defined by pathout
    #we assume that an extentlayer is provided, which is used to provide information on raster origin
    #extentlayer is assumed to be the same as when reading the flood grid
    import arcpy
    status=1
    try:
        pixelWidth = pixelHeight = resolution # depending how fine you want your raster
        x_min, x_max, y_min, y_max = extentlayer.GetExtent()
        llcorner=arcpy.Point(x_min,y_min)
        no_data=0
        myRaster = arcpy.NumPyArrayToRaster(array,llcorner,pixelWidth,pixelHeight,no_data)
        myRaster.save(pathout)
        del myRaster
    except:
        return status
    status=0
    return status
    
#def gridding_numpy_array(points,values,extentlayer,resolution,nodataval):
#    #grid numpy array to different resolution using scipy
#    #extent of the raster is defined from extent of extentlayer, gridsize through parameter resolution
#    status=1
#    try:
#        pixelWidth = pixelHeight = resolution # depending how fine you want your raster
#        x_min, x_max, y_min, y_max = extentlayer.GetExtent()
#        cols = int((x_max - x_min) / pixelHeight)
#        rows = int((y_max - y_min) / pixelWidth)
#        grid_x, grid_y = numpy.meshgrid(numpy.arange(x_min+0.5*resolution,x_min+0.5*resolution+cols*resolution,resolution), numpy.arange(y_min+0.5*resolution,y_min+0.5*resolution+rows*resolution,resolution))
#        data = griddata(points, values, (grid_x, grid_y), method='nearest')
#        #data = numpy.flipud(data)
#        data[data==nodataval]=0
#    except:
#        return status, None
#    status=0
#    return status, data

def gridding_numpy_array(points,values,extentlayer,resolution,nodataval):
    #grid numpy array to different resolution using scipy
    #extent of the raster is defined from extent of extentlayer, gridsize through parameter resolution
    status=1
    try:
        pixelWidth = pixelHeight = resolution # depending how fine you want your raster
        x_min, x_max, y_min, y_max = extentlayer.GetExtent()
        cols = int((x_max - x_min) / pixelHeight)
        rows = int((y_max - y_min) / pixelWidth)
        rowsize=2500000.0/float(cols)
        divisions=round(float(rows)/rowsize)
        if divisions<=1.0:
            rowsize=rows
        else:
            rowsize=int(ceil(float(rows)/divisions))
        last=0
        #selector=points[1]>x_min-50 and points[1]<x_max+50 and points[2]>y_min-50 and points[2]<y_max+50
        selector=numpy.logical_and(points[0]>x_min-50,points[0]<x_max+50)
        selector=numpy.logical_and(selector,points[1]>y_min-50)
        selector=numpy.logical_and(selector,points[1]<y_max+50)
        values2=values[selector]
        points2=(points[0][selector],points[1][selector])
        while last<rows:
            print ('gridded '+str(last)+' out of '+str(rows))
            ystart=y_min+(0.5+float(last))*resolution
            norows=min(rowsize,rows-last)
            yend=y_min+0.5*resolution+(float(last)+norows)*resolution
            grid_x, grid_y = numpy.meshgrid(numpy.arange(x_min+0.5*resolution,x_min+(0.5+cols)*resolution,resolution)[0:cols], numpy.arange(ystart,yend,resolution)[0:norows]) #indexing to make sure rounding errors don't affect row number
            data = griddata(points2, values2, (grid_x, grid_y), method='nearest')
            last=last+rowsize
            if last==rowsize:
                dataout=numpy.empty_like(data)
                dataout[:]=data[:]
            else:
                dataout=numpy.concatenate((dataout,data))
        #data = numpy.flipud(data)
        dataout[dataout==nodataval]=0
    except:
        print('gridding_numpy_array failed')
        return status, None
    status=0
    return status, dataout



#def gridding_numpy_array(points,values,extentlayer,resolution,nodataval):
#    #grid numpy array to different resolution using scipy
#    #extent of the raster is defined from extent of extentlayer, gridsize through parameter resolution
#    status=1
#    try:
#        pixelWidth = pixelHeight = resolution # depending how fine you want your raster
#        x_min, x_max, y_min, y_max = extentlayer.GetExtent()
#        cols = int((x_max - x_min) / pixelHeight)
#        rows = int((y_max - y_min) / pixelWidth)
#        rowsize=2500000.0/float(cols)
#        divisions=round(float(rows)/rowsize)
#        if divisions<=1.0:
#            rowsize=rows
#        else:
#            rowsize=long(ceil(float(rows)/divisions))
#        last=0
#        import pdb
#        pdb.set_trace()
#        while last<rows:
#            ystart=y_min+(0.5+last)*resolution
#            yend=min([y_min+(0.5+last+rowsize)*resolution,y_min+0.5*resolution+rows*resolution])
#            print  (yend-ystart)/resolution
#            grid_x, grid_y = numpy.meshgrid(numpy.arange(x_min+0.5*resolution,x_min+(0.5+cols)*resolution,resolution), numpy.arange(ystart,yend,resolution))
#            data = griddata(points, values, (grid_x, grid_y), method='nearest')
#            last=last+rowsize
#            if last==rowsize:
#                dataout=numpy.empty_like(data)
#                dataout[:]=data[:]
#            else:
#                dataout=numpy.concatenate((dataout,data))
#        #data = numpy.flipud(data)
#        dataout[dataout==nodataval]=0
#    except:
#        return status, None
#    status=0
#    return status, dataout

def count_flooded_buildings_int2(floodraster,buildraster,thresh):
    #add flood and building rasters together, count how many buildings are flooded and save max water level for each building
    status=1
    #import time
    try:
        build_id=[] #id of flooded buildings
        flood_max=[] #max water level for flooded buildings
        floodraster=numpy.int32(floodraster*100)
        floodraster[floodraster>999]=999
        thresh=int(thresh*100)
        #evaluate for each building in flood raster if it is in an area with water level above the flooding threshold
        #print (numpy.amax(buildraster)-500000)/1000
        for i in range(500000,int(numpy.amax(buildraster)),1000): #assuming that the ID's in the building rasters where defined from 500000 upwards in steps of 1000
            if round((float(i)-500000.0)/100000.0,0)==((float(i)-500000.0)/100000.0): print(i/numpy.amax(buildraster)*100)
            #select=numpy.logical_and(addRaster>=i, addRaster<(i+1000))
            subset=floodraster[buildraster==i] #get number of subset cells for assessing building area?
            if subset.shape!=(0,):
                maxv=numpy.amax(subset)
            else:
                continue
            if maxv>thresh:
                #build_id.append((i-500000)/1000)
                build_id.append(i) #save the id used in bf_fid
                flood_max.append(maxv)
        flood_max=[float(x)/100 for x in flood_max]
    except:
        return status, None, None
    status=0
    return status, build_id, flood_max

def count_flooded_buildings(floodraster,buildraster,thresh):
    #count how many buildings are flooded above threshold, save their ID's and the max waterlevel around the building
    status=1
    #import time
    try:
        build_id=[] #id of flooded buildings
        flood_max=[] #max water level for flooded buildings
        #reshape rasters to 1D
        floodraster=numpy.reshape(floodraster,floodraster.shape[0]*floodraster.shape[1])
        buildraster=numpy.reshape(buildraster,buildraster.shape[0]*buildraster.shape[1])
        #select only values with flooding and buildings (remove all 0's)
        select=numpy.where(numpy.logical_and(floodraster>thresh,buildraster > 500000))
        floodraster=floodraster[select]
        buildraster=buildraster[select]
        #run through unique list of indices in buildraster, extract max for these, possibly sort array first for further speed up
        for i in numpy.unique(buildraster):
            subset=floodraster[numpy.where(buildraster==i)] #get number of subset cells for assessing building area?
            if subset.shape!=(0,):
                maxv=numpy.amax(subset)
            else:
                continue
            build_id.append(i) #save the id used in bf_fid
            flood_max.append(maxv)
        flood_max=[float(x) for x in flood_max]
    except:
        return status, None, None
    status=0
    return status, build_id, flood_max
    
    
def compute_flooded_area(floodraster,typeraster,thresh,resolution):
    #count number of street cells flooded above threshold and compute area
    status=1
    try:
        thresh=thresh
        select = numpy.logical_and(floodraster>thresh, typeraster >= 500000)
        area = numpy.sum(select)*(resolution**2)
    except:
        return status
    status=0
    return status, area
    
def get_builing_area(layer, idfield, idval):
    #go through all the buildings in layer
    #if the id specified by idfield is included in the list idval - get area of the object and append it to the output area list
    #used for extracting the footprint area of flooded commercial buildings
    status=1
    try:
        i=1
        fid=0
        list_area=[]
        idlist=[]
        fcount=layer.GetFeatureCount()
        while i <= fcount:
            feature=layer.GetFeature(fid)
            fid=fid+1
            if feature is None: continue
            feat_id=int(feature.GetField(idfield))
            idlist.append(feat_id)
            if feat_id in idval:
                poly=feature.GetGeometryRef()
                area=poly.GetArea()
                list_area.append(area)
            i=i+1
        layer.ResetReading()
    except:
        return status, None
    status=0
    return status, list_area

def create_damage_raster(res_id,res_dam,res_np_raster,com_id,com_dam,com_np_raster,resolution,floodraster,streetraster,dfrd):
    #create an output raster which in each cell contains the damage occuring in this cell, by 
    #dividing the damage for each residential and commercial building over the cells that the building covers
    #assigning the road damage to flooded road pieces
    dam_raster=numpy.zeros_like(floodraster,dtype=numpy.float)
    #residential buildings
    for i in range(len(res_id)):
        id=res_id[i]
        select=res_np_raster==id
        count=numpy.sum(select)
        damage=res_dam[i]/count #assign equal amount of damage to each cell covered by the building
        dam_raster[select]=dam_raster[select]+damage
    #commercial buildings
    for i in range(len(com_id)):
        id=com_id[i]
        select=com_np_raster==id
        count=numpy.sum(select)
        damage=com_dam[i]/count #assign equal amount of damage to each cell covered by the building
        dam_raster[select]=dam_raster[select]+damage       
    #roads
    select = numpy.logical_and(floodraster>dfrd[0][0], streetraster >= 500000) #find cells in street raster flooded above threshold in damage function
    dam_raster[select]=dam_raster[select]+dfrd[1][0]*(resolution**2) #assign unit damage to those cells according to grid resolution
    return dam_raster
