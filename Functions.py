"""
@file
@author  Roland Loewe <rolo@env.dtu.dk>
@version 1.1
@section LICENSE
Copyright (C) 2017 Roland Loewe
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""


import os
import xml.etree.ElementTree as ET

def writefile(path,text):
    #write text to the file in path, overwrite existing file
    f=open(path,'w')
    f.write(text)
    f.close()

def appendfile(path,text):
    #write text to the file in path, overwrite existing file
    f=open(path,'a')
    f.write(text)
    f.close()

def get_path_xml(record,item):
    tmp=record.find(item)
    path=os.path.abspath(tmp.find('path').text)
    return path

def get_typefield_xml(record,item):
    tmp=record.find(item)
    typefield=tmp.find('typefield').text
    return typefield
    
def read_data_config(path):
    file = open(path, "r")
    tree = ET.parse(file)
    file.close()
    root = tree.getroot()
    for record in root.findall("data"):
        respath=get_path_xml(record,'RESFILE')
        bpath=get_path_xml(record,'BUILDINGS')
        btypefield=get_typefield_xml(record,'BUILDINGS')
        rpath=get_path_xml(record,'ROADS')
        gpath=get_path_xml(record,'GREEN')
        epath=get_path_xml(record,'EXTENT')
        wpath=get_path_xml(record,'WORKPATH')
    return wpath,respath,bpath,rpath,gpath,epath,btypefield

def read_software_paths(path):
    file = open(path, "r")
    tree = ET.parse(file)
    file.close()
    root = tree.getroot()
    for record in root.findall("software"):
        msdkpath=get_path_xml(record,'MIKESDK')
    return msdkpath 

def unpack_damage_f(record,dtype):
    ddict={}
    for record1 in record.findall('type'):
        tag=record1.find('tag').text
        ftype=record1.find('dtype').text
        level=[float(x) for x in record1.find('level').text.split(';')]
        damage=[float(x) for x in record1.find(dtype).text.split(';')]
        ddict[tag]=[ftype,level,damage]
    return ddict
    
def read_damage_functions(path):
    #read damage functions
    file = open(path, "r")
    tree = ET.parse(file)
    file.close()
    root = tree.getroot()
    record=root.find("damagefunc")
    record1=record.find('BUILDINGS')
    dbuild=unpack_damage_f(record1,'damage')
    record1=record.find('ROADS')
    droad=unpack_damage_f(record1,'damage')
    record1=record.find('GREEN')
    dgreen=unpack_damage_f(record1,'damage')    
    return dbuild, droad, dgreen
    