"""
@file
@author  Roland Loewe <rolo@env.dtu.dk>
@version 1.0
@section LICENSE
Copyright (C) 2016 Roland Loewe
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""
import os, sys
import numpy

from osgeo import gdal,gdalconst,ogr,osr
import clr
clr.AddReference("DHI.Generic.MikeZero.DFS")
clr.AddReference("DHI.Generic.MikeZero.EUM")
clr.AddReference("System")

import System
from System import Array
from DHI.Generic.MikeZero import eumUnit, eumItem, eumQuantity
from DHI.Generic.MikeZero.DFS import *
from DHI.Generic.MikeZero.DFS.dfs123 import *
#from DHI.Generic.Cartography import *
from DHI.Generic.MikeZero.DFS.dfsu import *


def dfsu_coordinates_to_numpy_list(pathin):
    #extracts element point coordinates and saves them to lists
    status=1
    try:
        #directory where result files are created
        pathout=os.path.split(pathin)[0]
        #open dfsu file
        file=DfsuFile.Open(pathin)
        numberOfElements = file.NumberOfElements
        numberOfNodes = file.NumberOfNodes
        ElementTable=file.ElementTable
        NodeX = numpy.array(list(file.X))
        NodeY = numpy.array(list(file.Y))
        # Read dynamic item info
        firstItemName = file.ItemInfo[0].Name
        quantity = file.ItemInfo[0].Quantity
        nitem=file.ItemInfo.Count
        nstep=file.NumberOfTimeSteps
        file.Close()
        
        #compute coordinates for element centre points
        ElementX=list()
        ElementY=list()
        for i in range(numberOfElements):
            ElementNodes = numpy.array(list(ElementTable[i]))
            X=0
            Y=0
            count=0
            for index in ElementNodes: #node indices are saved starting from 1 in element table
                X=X+NodeX[index-1]
                Y=Y+NodeY[index-1]
                count=count+1
            ElementX.append(X/count)
            ElementY.append(Y/count)
        
        #combine Element coordinates into points
        points=(numpy.array(ElementX),numpy.array(ElementY))
    except:
        return status, None
    status=0
    return status, points

def dfsu_data_to_numpyarray_gdal(pathin,coordinates,srs,extentlayer,resolution):
    #read max flood from dfsu and convert to grid using GDAL rasterize
    status=1
    try:
        outdriver=ogr.GetDriverByName('MEMORY')
        src_flood=outdriver.CreateDataSource('tmpflood')
        tmp=outdriver.Open('tmpflood',1)
        flood = src_flood.CreateLayer("flood", srs, geom_type=ogr.wkbPoint)
        flood.CreateField(ogr.FieldDefn('level', ogr.OFTReal))
        
        #load data for the first item, 1st timestep
        file=DfsuFile.Open(pathin)
        itemTimeStepData = file.ReadItemTimeStep(1, 0).Data #assuming that we are only reading max flood level
        file.Close()
        values=numpy.array(list(itemTimeStepData))
        
        #add values as points to vector layer
        layerdef=flood.GetLayerDefn()
        for i in range(len(coordinates[0])):
            feature = ogr.Feature(layerdef)
            feature.SetField("level", values[i])
            point=ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(coordinates[0][i], coordinates[1][i])
            feature.SetGeometry(point)
            flood.CreateFeature(feature)
            feature.Destroy()
        #rasterize flood layer to same extent as buildings
        status1, dataarr = ogrlayer_to_array(flood,extentlayer,srs,resolution,"level",1e-035)
        src_flood.Destroy()
        src_flood=None
    except:
        return status
    status=0
    return status, dataarr

def dfsu_data_to_numpyarray(pathin,coordinates,extentlayer,resolution):
    #read max flood from dfsu and convert to grid using GDAL rasterize
    status=1
    try:
        #load data for the first item, 1st timestep
        file=DfsuFile.Open(pathin)
        itemTimeStepData = file.ReadItemTimeStep(1, 0).Data #assuming that we are only reading max flood level
        file.Close()
        values=numpy.array(list(itemTimeStepData))
        itemTimeStepData=None
        values=values*100
        values=numpy.int32(values)
        #rasterize flood layer to same extent as buildings
        status1, dataarr = gridding_numpy_array(coordinates,values,extentlayer,resolution,1.0e-35)
    except:
        return status
    status=0
    return status, dataarr
