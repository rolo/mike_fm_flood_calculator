"""
@file
@author  Roland Loewe <rolo@env.dtu.dk>
@version 1.1
@section LICENSE
Copyright (C) 2017 Roland Loewe
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import os,sys
import numpy
import shutil

#import clr
#clr.AddReference("DHI.Generic.MikeZero.DFS")
#clr.AddReference("DHI.Generic.MikeZero.EUM")
#clr.AddReference("System")

#import System
#from System import Array
#from DHI.Generic.MikeZero import eumUnit, eumItem, eumQuantity
#from DHI.Generic.MikeZero.DFS import *
#from DHI.Generic.MikeZero.DFS.dfs123 import *
from mikecore.DfsFactory import DfsBuilder, DfsFactory  # type: ignore
from mikecore.DfsFile import DfsFile, DfsSimpleType  # type: ignore
from mikecore.DfsFileFactory import DfsFileFactory  # type: ignore
from mikecore.eum import eumQuantity, eumUnit, eumItem  # type: ignore


#in c:\Python27\ArcGIS10.1\Scripts\: pip install pythonnet
#example codes on using OGR are in http://pcjericks.github.io/py-gdalogr-cookbook/index.html

def get_spatial_reference(filename):
    #from mikecore.DfsFileFactory import DfsFileFactory
    status=1
    dfs2File = DfsFileFactory.Dfs2FileOpen(filename)
    XCount = dfs2File.SpatialAxis.XCount
    YCount = dfs2File.SpatialAxis.YCount
    xysize = XCount * YCount
    Dx = dfs2File.SpatialAxis.Dx #5
    Dy = dfs2File.SpatialAxis.Dy #5
    X0 = dfs2File.SpatialAxis.X0 #0
    Y0 = dfs2File.SpatialAxis.Y0 #0
    Lat = dfs2File.FileInfo.Projection.Latitude #-37.89519109801612
    Lon = dfs2File.FileInfo.Projection.Longitude #145.1455131881479
    Or = dfs2File.FileInfo.Projection.Orientation #1.13931185349487
    WKT = dfs2File.FileInfo.Projection.WKTString
    dfs2File.Close()
    return [status,XCount,YCount,xysize,Dx,Dy,X0,Y0,Lat,Lon,Or,WKT]

def get_extent(WKT,Lon,Lat,Or, Dx, Dy, XCount, YCount):
    import arcpy
    status=1
    #create a geometry, project it and extract projected coordinates of the grid origin
    spr=arcpy.SpatialReference()
    spr.loadFromString(WKT)
    sprgcs=arcpy.SpatialReference(spr.GCS.GCSCode)
    string="POINT (" + str(Lon) + " " + str(Lat) +")"
    geom=arcpy.FromWKT(string, sprgcs)
    geomproj=geom.projectAs(spr)
    #check if the grid is rotated (not used so far)
    status=check_grid_rotation(spr,geom,sprgcs,geomproj,Or)
    #compute grid extent when (Lon,Lat) is placed in the centre of the bottom left cell of the dfs2
    [blX,blY,trX,trY]=[geomproj.extent.XMin-Dx/2,geomproj.extent.YMin-Dy/2,geomproj.extent.XMin-Dx/2+XCount*Dx,geomproj.extent.YMin-Dy/2+YCount*Dy]
    return [status,blX,blY,trX,trY]

def get_extent_GDAL(WKT,Lon,Lat,Or, Dx, Dy, XCount, YCount):
    from osgeo import ogr,osr
    status=1
    target = osr.SpatialReference()
    target.ImportFromWkt(WKT)
    source = target.CloneGeogCS()
    transform = osr.CoordinateTransformation(source, target)
    #create a geometry, project it and extract projected coordinates of the grid origin
    string="POINT (" + str(Lon) + " " + str(Lat) +")"
    point = ogr.CreateGeometryFromWkt(string)
    point.Transform(transform)
    #compute grid extent when (Lon,Lat) is placed in the centre of the bottom left cell of the dfs2
    [blX,blY,trX,trY]=[point.GetX()-Dx/2,point.GetY()-Dy/2,point.GetX()-Dx/2+XCount*Dx,point.GetY()-Dy/2+YCount*Dy]
    return [status,blX,blY,trX,trY]
    
def check_grid_rotation(spr,geom,sprgcs,geomproj,Or):
    import arcpy
    status=1
    #compute grid convergence in the projected system, compare this to Or, to see if the grid is rotated
    #calculate grid convergence
    TMP="in_memory/TMP"
    arcpy.CreateFeatureclass_management("in_memory", "TMP", "POINT", "", "DISABLED", "DISABLED", spr)
    arcpy.AddField_management(TMP,"conv","Double")
    cursor = arcpy.da.InsertCursor(TMP,["SHAPE@XY"])
    cursor.insertRow([(geomproj.extent.XMin,geomproj.extent.YMin)])
    del cursor
    arcpy.CalculateGridConvergenceAngle_cartography (TMP, "conv", "", "")
    cursor = arcpy.da.SearchCursor(TMP,["conv"])
    row=cursor.next()
    del cursor
    arcpy.Delete_management(TMP)
    conv=row[0] #convergence is negative but otherwise the same, seems to use different counting than MIKE Urban (although the manuals both claim that they count clockwise from top)
    #by comparing conv and Or from dfs2 we can now check if the grid is rotated and adjust our output in this case
    if (1-abs(conv/Or))>1e-05: print("ROTATED GRID")
    if (1-abs(Or/conv))>1e-05: print("ROTATED GRID")
    return status

def read_dfs2_timestep(file, index, timestep):
    status=1
    dfs2File = DfsFileFactory.Dfs2FileOpen(file)
    dfsdata = dfs2File.ReadItemTimeStep(index, timestep) #item no, time step
    dfs2File.Close()
    return [status, dfsdata]
    
def dfsdata_to_numpy1D(dfsdata,XCount,YCount):
    #take a data item from dfs2 file and convert it to a 1D-numpy array which in this form can be written back to dfs
    status=1
    #dfsdata_np=numpy.array(list(dfsdata.Data) )#convert the dfs data to numpy array for easier processing
    dfsdata_np=dfsdata.Data
    #data are now column wise in the 1D-array (where bottom is the first and top the last value))
    dfsdata_np2=numpy.reshape(dfsdata_np,(XCount,YCount),order='C')
    dfsdata_np3=numpy.reshape(dfsdata_np2,XCount*YCount, order='F')
    return [status, dfsdata_np3]
    
def dfsdata_to_numpy2D(dfsdata,XCount,YCount):
    #take a data item from dfs2 file and convert it to a 2D-numpy array which can be converted to ArcGIS raster
    status=1
    #tmp=numpy.array(list(dfsdata.Data))
    tmp=dfsdata.Data
    tmp=numpy.reshape(tmp,(XCount,YCount),order='F')
    tmp=numpy.rot90(tmp, k=1)
    return [status, tmp]
    
