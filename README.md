# General info
* an Anaconda environment that works with the tool is included in ./Anaconda
* testing data (flood maps and landuse shape files) are included in ./Data

# To run
* open Anaconda Prompt
* check which config file is being referred to in line 35 of Main_Postprocessor.py (p_config=os.path.join(wd,'config','config_damage_dfs2.xml'))
* adjust the paths inside the config file (which flood statistics file should be read, which building data, which extent polygon etc.)
* change active path in comment prompt to directory where Main_Postprocessor.py is located (e.g. "cd c:\Documents\users\rolo\Desktop\Damagetool")
* type "python Main_Postprocessor.py"

# current test environment:
* Python 3.10.8
* MIKE-IO 1.6.3