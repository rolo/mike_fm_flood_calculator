"""
@file
@author  Roland Loewe <rolo@env.dtu.dk>
@version 1.1
@section LICENSE
Copyright (C) 2017 Roland Loewe
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import os
from osgeo import ogr

def open_db(dbpath):
    import sqlite3
    conn=sqlite3.connect(dbpath)
    conn.enable_load_extension(True)
    conn.execute("SELECT load_extension('mod_spatialite')")
    return conn
    
def replace_subcatch(pcatchdb,pdancedb,psqlite):
    #replace subcatchments in dancedb by those in template, using a MULTIPOLYGON geometry
    os.environ["PATH"] = psqlite + os.pathsep + os.environ["PATH"]
    import sqlite3
    conn=open_db(pdancedb)
    try:
        #drop existing subcatchments
        conn.execute("SELECT DropGeoTable('sub_catchment')")
        #create new subcatchment table
        conn.execute("CREATE TABLE sub_catchment(ogc_fid INTEGER PRIMARY KEY AUTOINCREMENT, subcatch_id INTEGER)")
        conn.execute("SELECT AddGeometryColumn('sub_catchment','GEOMETRY',32755,'MULTIPOLYGON',2)")
        #attach the template database
        conn.execute("ATTACH DATABASE '"+ pcatchdb +"' as 'TMP'")
        conn.execute("INSERT INTO main.sub_catchment(subcatch_id,GEOMETRY) SELECT subcatch_i,GEOMETRY FROM TMP.tmp_catch")
        conn.execute("DETACH DATABASE 'TMP'")
        conn.execute("VACUUM")
        conn.commit()
        conn.close()
    except:
        return 1
    return 0

def float_robust(x): 
    if not x is None: return float(x)
    else: return None


def clip_buildings(src,blyr,extlyr):
    #intersects catchment hull and building layer from DANCE, finds those buildings that are inside the catchment and puts these into a separate layer for further analysis
    #CLIP LOOSES THE SPATIAL REFERENCE, SO THIS NEEDS TO BE CONSIDERED DURING ANY HANDLING OF THE LAYER LATER!!!
    status=1
    try:
        build_all=src.GetLayer(blyr)
        catch_hull=src.GetLayer(extlyr)
        sref = build_all.GetSpatialRef()
        
        #create layer for buildings inside the catchment (after clipping)
        clip=src.CreateLayer('building',sref,ogr.wkbPolygon)
        #clip building layer
        build_all.Clip(catch_hull,clip)
        
        #reset read pointer
        clip.ResetReading() 
        build_all.ResetReading()
        catch_hull.ResetReading()
    except:
        return status
    status=0
    return status
