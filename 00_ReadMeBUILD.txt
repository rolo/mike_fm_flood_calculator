#need to have GDAL2.1.2 installed, otherwise there will be an osgeo help error
#possibly use newest version of pyinstaller by doing 
#pip install git+https://github.com/pyinstaller/pyinstaller


pyinstaller --onefile --hidden-import=psutil --hidden-import=clr --hidden-import=scipy.linalg --hidden-import=scipy.linalg.cython_blas --hidden-import=scipy.linalg.cython_lapack --hidden-import=scipy.linalg._decomp_update --hidden-import=preprocessing --hidden-import=Functions -F Main_Postprocessor.py

does not work with other ArcGIS version...